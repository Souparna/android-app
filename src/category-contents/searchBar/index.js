import { View, Text, TextInput, FlatList } from 'react-native'
import React, { useState } from 'react'
import styles from './styles'
import SearchFilter from '../searchFilter'

const SearchBar = ({data}) => {

  const [input, setInput]=useState("")
  return (
    <View style={styles.searchInput}>
      <TextInput  value={input} onChange={(e)=> setInput(e)} style={styles.searchBar} placeholder="Search"></TextInput>
      <SearchFilter input={input} setInput={setInput} data={data}/>
      
    </View>
  )
}

export default SearchBar