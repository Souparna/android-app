import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'


const styles = StyleSheet.create({

    searchInput:{
        position:"absolute",
        top:170,
        width:"100%",
        zIndex:999,
        display:"flex",
        alignItems:"center",
        justifyContent:"center",
    },
    searchBar:{
        width:"95%",
        paddingVertical:8,
        paddingHorizontal:12,
        backgroundColor:"#fff",
        borderRadius:13,
    },


})

export default styles