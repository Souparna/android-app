import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'


const styles = StyleSheet.create({

    eventWrapper:{
        position:"absolute",
        top:140,
        width:"100%",
        zIndex:999,
        display:"flex",
        alignItems:"center",
        justifyContent:"center",
    },
    colorwhite:{
        color:"#fff"
    },
    zipCodeBox:{
        width:"95%",
        paddingVertical:10,
        textAlign:"center",
        backgroundColor:"#fff",
        borderRadius:13,

    },
    zipCode:{
        textAlign:"center",
        color:"#3E3E3E"
    },
    Select:{
        width:"95%",
        backgroundColor:"#fff",
        paddingVertical:12,
        marginTop:10,
        borderRadius:13,
    },
    selectTitle:{
        marginLeft: 4,
        marginBottom:10,
        color:"#3E3E3E"
    },
    selectives:{
        display:"flex",
        width:"100%",
        alignItems:"center",
        justifyContent:"space-around",
        flexWrap:"wrap",
        flexDirection:"row"

    },
    selectiveButtons:{
        paddingVertical:6,
        paddingHorizontal:6,
        borderWidth:0.4,
        marginTop:8,
        borderRadius:13,
    },
    searchBar:{
        paddingHorizontal:6,
        paddingVertical:6,
        backgroundColor:"#fff",
        marginTop:8,
        width:"95%",
        borderRadius:13,
    },
    endButtons:{
        width:"95%",
        display:"flex",
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"space-around",
        marginTop:12,
    },
    endbutton:{
        paddingVertical:12,
        paddingHorizontal:32,
        marginTop:8,
        borderRadius:13,
        backgroundColor:"#18ACFE",
        color:"#fff"
    },
    endbutton2:{
        paddingVertical:12,
        paddingHorizontal:32,
        marginTop:8,
        borderRadius:13,
        backgroundColor:"#fff",
        color:"#fff"
    }


})

export default styles