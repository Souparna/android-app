import { View, Text, TouchableOpacity, TextInput } from 'react-native'
import React from 'react'
import styles from "./styles.js"

const Events = () => {
  return (
    <View style={styles.eventWrapper}>
      <View style={styles.zipCodeBox}>
        <Text style={styles.zipCode}>Zip Code, City, State</Text>
      </View>
      <View style={styles.Select}>
        <Text style={styles.selectTitle}>Select Events</Text>
        <View style={styles.selectives}>
          <TouchableOpacity style={styles.selectiveButtons}>
            <Text>Sports Events</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.selectiveButtons}>
            <Text>Social Events</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.selectiveButtons}>
            <Text>Summits</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.selectiveButtons}>
            <Text>Training Events</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.selectiveButtons}>
            <Text>Others</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.Select}>
        <Text style={styles.selectTitle}>Select Events</Text>
        <View style={styles.selectives}>
          <TouchableOpacity style={styles.selectiveButtons}>
            <Text>Sports Events</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.selectiveButtons}>
            <Text>Social Events</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.selectiveButtons}>
            <Text>Summits</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.selectiveButtons}>
            <Text>Training Events</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.selectiveButtons}>
            <Text>Others</Text>
          </TouchableOpacity>
        </View>
      </View>
      <TextInput placeholder='Search' style={styles.searchBar}></TextInput>
      <View style={styles.endButtons}>
        <TouchableOpacity style={styles.endbutton}>
          <Text style={styles.colorwhite}>Sports Events</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.endbutton2}>
          <Text>Social Events</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default Events