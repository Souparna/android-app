const data = [
    {
        id: 1,
        name: "Fab"
    },
    {
        id: 2,
        name: "Fabulas"
    },
    {
        id: 3,
        name: "Fabrics"
    },
    {
        id: 4,
        name: "Fab"
    },
    {
        id: 5,
        name: "Fab"
    },
    {
        id: 6,
        name: "Fab"
    },
    {
        id: 7,
        name: "Fab"
    },
    {
        id: 8,
        name: "Fab"
    },
]

const jewelry = [
    {
        id: 1,
        name: "Gold jwelry",
    },
    {
        id: 2,
        name: "Silver jwelry",
    },
    {
        id: 3,
        name: "Metal jwelry",
    },
    {
        id: 4,
        name: "Fabric jwelry",
    },
    {
        id: 5,
        name: "Other",
    },
]
const tailor = [
    {
        id: 1,
        name: "Men",
    },
    {
        id: 2,
        name: "Women",
    },
    {
        id: 3,
        name: "Kids",
    },
    {
        id: 4,
        name: "Botiques",
    },
    {
        id: 5,
        name: "Other(tailoring)",
    },
]
const others = [
    {
        id: 1,
        name: "Puja Items",
    },
    {
        id: 2,
        name: "Toys",
    },
    {
        id: 3,
        name: "Furniture",
    },
    {
        id: 4,
        name: "Home Decor",
    },
    {
        id: 5,
        name: "Footwear",
    },
    {
        id: 6,
        name: "Party Halls",
    },
]

export { data, jewelry, tailor , others}