import { View, Text, FlatList } from 'react-native'
import React from 'react'
import { data } from '../SearchData/data'
import styles from '../searchFilter/styles'

const SearchFilter = ({ input, setInput, data }) => {
    return (
        <View>
            <View style={styles.filterWrapper}>
                <FlatList data={data} renderItem={({ item }) => {
                    if (item === "") {
                        return (
                            <Text style={styles.searchFilter}>
                                {item.name}
                            </Text>
                        )
                    }
                    if (item.name.toLowerCase().includes(input.toLowerCase())) {
                        return (
                    
                                <Text style={styles.searchFilter}>
                                    {item.name}
                                </Text>
                        
                        )
                    }
                }} />
            </View>

        </View>
    )
}

export default SearchFilter