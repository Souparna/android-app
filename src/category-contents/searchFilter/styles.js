import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'


const styles = StyleSheet.create({

    filterWrapper:{
        borderRadius:12,
        backgroundColor:"#fff",
        paddingHorizontal:10,
        marginTop:8,
        width:340,
    },

    searchFilter: {
        
        width:"95%", 
        paddingVertical:6,
    }


})

export default styles