import { View, Text, Image } from 'react-native'
import React from 'react'
import Disabledstar from "../../../assets/disabledstar.png"
import Halfstar from "../../../assets/halfstar.png"
import Fullstar from "../../../assets/fullstar.png"
import styles from './styles'



const Ratings = ({stars}) => {
    const RatingStar = Array.from({ length: 5 }, (elem, index) => {
        let number = index + 0.5;

        return <View key={index} >
            {
                stars >= index + 1 ? (<Image style={styles.stars} source={Fullstar} />): stars>= number ? (<Image style={styles.stars}  source={Halfstar} />):(<Image style={styles.stars}  source={Disabledstar} />) 
            }
        </View>
    })
    return (
        <View style={styles.ratingStars}>
            {RatingStar} 
            <Text style={styles.ratingText}>{stars}</Text>
        </View>
    )
}

export default Ratings