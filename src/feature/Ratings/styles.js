import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'

const styles = StyleSheet.create({
    stars: {
        height: Device.isTablet ? 13 : 8,
        width: Device.isTablet ? 13 : 8,
        marginHorizontal: 2,
    },
    ratingStars: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
    },
    ratingText: {
        fontSize: Device.isTablet ? 15 : 10,
    },

})

export default styles