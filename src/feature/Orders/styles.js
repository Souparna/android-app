import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'

const styles = StyleSheet.create({
    item: {
        marginVertical: 20,
    },
    itemInfo: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    itemtextWrapper: {
        width: "70%",
        marginBottom: 10,
        borderBottomWidth: 0.4,
        borderStyle: "dashed",


    },
    itemText: {
        width: Device.isTablet ? 320 : 220,
        marginBottom: 30,
        fontSize: Device.isTablet ? 20 : 15,
        fontWeight: 500,
    },
    priceCounts: {
        marginRight: 10,
        display: "flex",
        alignItems: "center",
    },
    price: {
        fontWeight: 500,
        fontSize: 17,
    },

    itemDetails: {
        marginTop: 10,
    },
    itemDetailsText: {
        fontSize: Device.isTablet ? 14 : 12,
        width: "90%",
    },
    Counter: {
        width: 75,
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-around",
        borderWidth: 0.5,
        borderRadius: 10,
        paddingHorizontal: 4,
        paddingVertical: 6,
        marginTop: 8,
        borderColor: "#3C50E0",
        backgroundColor: "#3C50E026",
    },
    iconCounter: {
        height: 14,
        width: 14,

    },

})

export default styles