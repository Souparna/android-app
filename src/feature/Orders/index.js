import { View, Text, Image } from 'react-native'
import styles from "./styles"
import plus from '../../../assets/plus.png'
import minus from '../../../assets/minus.png'
import React, { useState } from 'react'

const Orders = ({orderDesc, orderTitle}) => {
    const [count, setCount] = useState(1)
    const [price, setPrice] = useState(100)

    const pluscount = () => { 
        setCount((val) => val + 1)
        setPrice((item) => item + 100)

    }
    const minuscount = () => {
        setCount((val) => (val === 1 ? val : val - 1))
        setPrice((item) => (item === 100 ? item : item - 100))


    }
    return (
        <View>
            <View style={styles.item}>
                <View style={styles.itemInfo}>
                    <View style={styles.itemtextWrapper}>
                        <Text style={styles.itemText}>{orderTitle}</Text>
                    </View>
                    <View style={styles.priceCounts}>
                        <Text style={styles.price}>{'\u20B9'}{price}</Text>
                        <View style={styles.Counter}>
                            <Text onPress={pluscount}>
                                <Image source={plus} style={styles.iconCounter} />
                            </Text>
                            <Text>{count}</Text>
                            <Text onPress={minuscount}>
                                <Image source={minus} style={styles.iconCounter} />
                            </Text>
                        </View>
                    </View>
                </View>
                <View style={styles.itemDetails}>
                    <Text style={styles.itemDetailsText}>{orderDesc}</Text>
                </View>
            </View>
        </View>
    )
}

export default Orders