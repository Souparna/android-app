import acservice from "../../assets/acservice.png"
import bath from "../../assets/bath.png"
import frame from "../../assets/frame.png"
import electrician from "../../assets/electrician.png"
import hair from "../../assets/hair.png"
import homepaint from "../../assets/homepaint.png"
import laptop from "../../assets/laptop.png"
import microwave from "../../assets/microwave.png"
import plumber from "../../assets/plumber.png"
import deepclean from "../../assets/deepcleanac.png"
import ACSERVICE from "../../assets/serviceac.png"
import PLUMBER from "../../assets/eplumber.png"
import APPLIENCES from "../../assets/appliences.png"
import HeadMessage from "../../assets/headmessage.png"
import HairColor from "../../assets/haircolor.png"
import Ratings from "../feature/Ratings/index"
import hairprice from "../../assets/hairstartingprice.png"
import waxprice from "../../assets/waxstartingprice.png"
import Fimage from '../../assets/Fimage.png'
import Fimage2 from '../../assets/Fimage2.png'
import Star from '../../assets/Star.png'
import Alert from "../../assets/Alert.png"
import vieweye from "../../assets/vieweye.png"
import WPicon from "../../assets/WPicon.png"



const services = [
    {
        id: "1",
        photo: frame,
        title: "Furniture Assembly",

    },
    {
        id: "2",
        photo: acservice,
        title: "AC Service and Repair"
    },
    {
        id: "3",
        photo: bath,
        title: "Bathroom and Kitchen Cleaning"
    },
    {
        id: "4",
        photo: hair,
        title: "Hair Studio for Women"
    },
    {
        id: "5",
        photo: homepaint,
        title: "Home Painting"
    },
    {
        id: "6",
        photo: electrician,
        title: "Electricians"
    },
    {
        id: "7",
        photo: laptop,
        title: "Laptop Repair"
    },
    {
        id: "8",
        photo: plumber,
        title: "Plumbers"
    },
    {
        id: "9",
        photo: microwave,
        title: "Microwave Repair"
    },
    {
        id: "10",
        photo: laptop,
        title: "Laptop Repair"
    },
    {
        id: "11",
        photo: plumber,
        title: "Plumbers"
    },
    {
        id: "12",
        photo: microwave,
        title: "Microwave Repair"
    }

];



const mostbooked = [
    {
        id: '1',
        photo: deepclean,
        title: "Deep clean AC service (split)",
        rate: <Ratings stars={3} />,
        price: "1000"
    },
    {
        id: '2',
        photo: deepclean,
        title: "Deep clean AC service (split)",
        rate: <Ratings stars={4} />,
        price: "1000"
    },
    {
        id: '3',
        photo: deepclean,
        title: "Deep clean AC service (split)",
        rate: <Ratings stars={1.5} />,
        price: "1000"
    },
    {
        id: '4',
        photo: deepclean,
        title: "Deep clean AC service (split)",
        rate: <Ratings stars={2} />,
        price: "1000"
    },
    {
        id: '5',
        photo: deepclean,
        title: "Deep clean AC service (split)",
        rate: <Ratings stars={3} />,
        price: "1000"
    },
    {
        id: '6',
        photo: deepclean,
        title: "Deep clean AC service (split)",
        rate: <Ratings stars={5} />,
        price: "1000"
    },
    {
        id: '7',
        photo: deepclean,
        title: "Deep clean AC service (split)",
        rate: <Ratings stars={3} />,
        price: "1000"
    },
]
const Quickrepair = [
    {
        id: '1',
        photo: electrician,
        title: "Deep clean AC service (split)",
        rate: <Ratings stars={5} />,
        price: "1000"
    },
    {
        id: '2',
        photo: bath,
        title: "Deep clean AC service (split)",
        rate: <Ratings stars={3.5} />,
        price: "1000"
    },
    {
        id: '3',
        photo: deepclean,
        title: "Deep clean AC service (split)",
        rate: <Ratings stars={2.5} />,
        price: "1000"
    },
    {
        id: '4',
        photo: bath,
        title: "Deep clean AC service (split)",
        rate: <Ratings stars={4.5} />,
        price: "1000"
    },
    {
        id: '5',
        photo: deepclean,
        title: "Deep clean AC service (split)",
        rate: <Ratings stars={3} />,
        price: "1000"
    },
    {
        id: '6',
        photo: deepclean,
        title: "Deep clean AC service (split)",
        rate: <Ratings stars={5} />,
        price: "1000"
    },
    {
        id: '7',
        photo: electrician,
        title: "Deep clean AC service (split)",
        rate: <Ratings stars={3} />,
        price: "1000"
    },
]


const cleancontrol = [
    {
        id: '1',
        photo: bath,
        title: "Bathroom And Kitchen Cleaning",
    },
    {
        id: '2',
        photo: homepaint,
        title: "Full Home cleaning",
    },
    {
        id: '3',
        photo: frame,
        title: "Sofa and Carpet Cleaning",
    },
]


const categorysections = [
    {
        id: '1',
        photo: ACSERVICE,
        title: "Ac services and Repair",
    },
    {
        id: '2',
        photo: APPLIENCES,
        title: "Appliance Repair and services",
    },
    {
        id: '3',
        photo: PLUMBER,
        title: "Electrician Plumber and Carpenters",
    },
    {
        id: '4',
        photo: APPLIENCES,
        title: "Electrician Plumber and Carpenters",
    },
    {
        id: '5',
        photo: PLUMBER,
        title: "Electrician Plumber and Carpenters",
    },
    {
        id: '6',
        photo: PLUMBER,
        title: "Electrician Plumber and Carpenters",
    },
]

const FrequentProducts = [
    {
        id: '1',
        photo: HairColor,
        title: "Hair Color/mehendi",
        price:  100,
        btn: "Add",
    },
    {
        id: '2',
        photo: HeadMessage,
        title: "Head Message",
        price: 100,
        btn: "Add",
    },
    {
        id: '3',
        photo: HairColor,
        title: "Hair Color/mehendi",
        price: 100,
        btn: "Add"
    },
    {
        id: '4',
        photo: HeadMessage,
        title: "Head Message",
        price: 100,
        btn: "Add"
    },
]

const Banners = [
    {
        photo: hairprice,
    },
    {
        photo:waxprice,
    },
    {
        photo: hairprice,
    },
    {
        photo:waxprice,
    },
]

const orders = [
    {
        title:"Full arms + full legs + underarms waxing",
        desc:"Lorem ipsum dolor sit amet. At repellendus recusandae qui dolores tempora sed illo alias est dolores itaque"
    },
    {
        title:"Full arms + full legs + underarms waxing",
        desc:"Lorem ipsum dolor sit amet. At repellendus recusandae qui dolores tempora sed illo alias est dolores itaque"
    },
]

const slots = [
    {
        title:"8:30"
    },
    {
        title:"9:30"
    },
    {
        title:"11:30"
    },
    {
        title:"8:30"
    },
    {
        title:"12:30"
    },
    {
        title:"13:30"
    },
    {
        title:"8:30"
    },
    {
        title:"7:30"
    },
    {
        title:"6:30"
    },
    {
        title:"8:30"
    },
    {
        title:"10:30"
    },
    {
        title:"8:30"
    },
]

const fashion = [
    {
        id:1,
        verify:"Verified",
        image:Fimage,
        title:"Fashion Mantra",
        tag: Star,
        mode:"Festive mode....",
        hastag:"#New Trend 23",
        productid: "FM-123-2",
        cost:"100 Rs",
        view: vieweye,
        wp:WPicon,
        alert: Alert,

    },
    {
        id:2,
        verify:"Verified",
        image: Fimage2,
        verified:",",
        title:"Fashion Mantra",
        tag:Star,
        mode:"Festive mode....",
        hastag:"#New Trend 23",
        productid: "FM-123-2",
        cost:"100 Rs",
        view: vieweye,
        wp:WPicon,
        alert: Alert,

    },
]

export { services,  cleancontrol, categorysections,  FrequentProducts, Banners, mostbooked, Quickrepair, orders, slots, fashion};