import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'

const styles = StyleSheet.create({
    accountWrapper: {
        marginTop: 40,
    },
    linkColor: {
        color: "#3C50E0",
        fontWeight: 500,
    },
    accountHead: {
        marginTop: 20,
        marginBottom: 20,
        borderBottomColor: "#ddd",
        borderBottomWidth: 1,
    },
    leftArrow: {
        height: 25,
        width: 25,
        marginLeft: 20,
        marginBottom: 5,
    },
    headText: {
        fontSize: 13,
        marginLeft: 20,
        marginBottom: 12,

    },
    packageWrapper: {
        marginLeft: 20,
        marginRight: 15,
    },
    packageHead: {
        fontSize: Device.isTablet ? 28 : 18,
        fontWeight: 600,
        marginBottom: 30,
    },
    servicePreferences: {
        marginLeft: 20,
        marginBottom: 40,
    },
    serviceprefHead: {
        fontSize: 20,
        marginVertical: 35,
        fontWeight: 500,
    },
    serviceprefCheckbox: {
        color: "#ddd",
        marginBottom: 20,
    },
    serviceprefInstinputs: {
        width: Device.isTablet ? "90%" : 300,
        paddingHorizontal: 12,
        paddingVertical: 6,
        display: "flex",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "space-between",
        borderWidth: 0.4,
        borderColor: "#ddd",
        borderRadius: 4,
        marginTop: 8,
    },
    offer: {
        display: "flex",
        alignItems: "center",
        justifyContent: "space-around",
        flexDirection: "row",
        paddingVertical: 30,
        backgroundColor: "#F8F8F8",
        marginBottom: 40,
    },

    offerTexthead: {
        fontWeight: 500,
    },
    paymentSummary: {
        marginLeft: 20,
        marginBottom: 30,
    },
    paymentSummaryHead: {
        fontSize: 20,
        fontWeight: 500,
        marginBottom: 30,
    },
    paymentSummaryDetails: {
        width: 300,
        borderBottomWidth: 0.5,
        borderStyle: "dashed",
        borderColor: "#ddd"
    },
    paymentSummaryTotal: {
        width: 300,
        marginTop: 20,
        marginBottom: 10,
    },
    paymentCalc: {
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        flexDirection: "row",
        marginBottom: 15,

    },
    paymentCalcItem: {
        fontSize: 17,
        fontWeight: 500,
    },
    addAddress: {
        width: "100%",
        display: "flex",
        alignItems: "center",

    },
    addAddressBtn: {
        width: "90%",
        backgroundColor: "#3C50E0",
        paddingVertical: 12,
        textAlign: "center",
        borderRadius: 12,
        marginBottom: 20,
    },
    addaddressBtnText: {
        color: "#fff",
        textAlign: "center",
    },
    addressHead: {
        marginVertical: 20,
        marginLeft: 10,
        width: "90%"
    },
    servicepref_instinputs: {
        width: Device.isTablet ? "90%" : 300,
        paddingHorizontal: 12,
        paddingVertical: 6,
        display: "flex",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "space-between",
        borderWidth: 0.4,
        borderColor: "#ddd",
        borderRadius: 4,
        marginTop: 8,
    },
    cardWrap:{
        height:75,
        borderWidth:0.2,
        borderColor:"#ddd",
        display:"flex",
        alignItems:"flex-start"
    }

})

export default styles