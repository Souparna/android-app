import { View, Text, Image, TextInput, FlatList, TouchableOpacity, ScrollView } from 'react-native'
import React from 'react'
import Orders from '../../feature/Orders'
import { FrequentProducts, orders } from '../../Data/data'
import styles from './styles'
import leftarrow from "../../../assets/leftarrow.png"
import Discount from "../../../assets/discount.png"
import Scrollcard from '../../containers/Cards/Scrollcard'


const Account = ({navigation}) => {  
    return (
        <>
        <ScrollView>
            <View style={styles.accountWrapper}>
                <View style={styles.accountHead}>
                    <Image style={styles.leftArrow} source={leftarrow} onPress={() => navigation.navigate("Category")} />
                    <Text style={styles.headText}>You,re saving total {'\u20B9'}100 on this order!</Text>
                </View>
                <View style={styles.packageWrapper}>
                    <Text style={styles.packageHead}>Your Package</Text>
                    {orders.map((items) => {
                        return (
                            <Orders orderTitle={items.title} orderDesc={items.desc} />
                        )
                    })}


                    <View style={styles.servicelayout}>
                        <FlatList
                            horizontal
                            data={FrequentProducts}
                            renderItem={(elem) => {
                                return <Scrollcard cardWrapper={styles.cardWrap} cardbutton={styles.cardButton}  ImageSource={elem.item.photo}  title={elem.item.title}  Price={elem.item.price}  />
                            }}
                        />
                    </View>
                </View>
                <View style={styles.servicePreferences}>
                    <Text style={styles.serviceprefHead}>Service Preferences</Text>
                    <Text style={styles.serviceprefCheckbox}>
                        Avoiding calling before reaching the location</Text>
                    <View>
                        <Text style={styles.serviceprefInstructions}>Add Instructions</Text>
                        <View style={styles.serviceprefInstinputs}>
                            <TextInput placeholder='Message' />
                            <Text style={styles.linkColor}>Send</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.offer}>
                    <Image source={Discount} />
                    <View style={styles.offerTexts}>
                        <Text style={styles.offerTexthead}>SAVE 15% OFF ON EVERY BOOKING</Text>
                        <Text>Get Plus Now</Text>
                    </View>
                    <Text style={styles.linkColor}>6 offers</Text>
                </View>
                <View style={styles.paymentSummary}>
                    <Text style={styles.paymentSummaryHead}>Payment Summery</Text>
                    <View style={styles.paymentSummaryDetails}>
                        <View style={styles.paymentCalc}>
                            <Text style={styles.paymentCalcItem}>Item Total</Text>
                            <Text> {'\u20B9'} 1,590</Text>
                        </View>
                        <View style={styles.paymentCalc}>
                            <Text style={styles.paymentCalcItem}>Membership Discount</Text>
                            <Text> {'\u20B9'} 10</Text>
                        </View>
                    </View>
                    <View style={styles.paymentSummaryTotal}>
                        <View style={styles.paymentCalc}>
                            <Text style={styles.paymentCalcItem}>TOTAL</Text>
                            <Text>{'\u20B9'} 1600</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.addAddress}>
                    <TouchableOpacity onPress={() => navigation.navigate("Address")} style={styles.addAddressBtn}>
                        <Text style={styles.addaddressBtnText}>Add Address and Slot</Text>
                    </TouchableOpacity>
                </View>
            </View >
            </ScrollView>

        </>
    )
}

export default Account