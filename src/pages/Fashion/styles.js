import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'
import { Dimensions } from "react-native";

const {height, width}= Dimensions.get('screen')

const styles = StyleSheet.create({
    fashionBackground:{
        height,
        width,
        display:"flex",
        alignItems:"center",
        justifyContent:"center"
    },
    texts:{
        position:"absolute",
        bottom:40,
    }

})

export default styles