import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    herobgWrapper: {
        height: Device.isTablet ? 420 : 300,
        position: "relative",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",

    },
    heroTexts: {
        color: "#fff",
        fontSize: Device.isTablet ? 29 : 18,
        textAlign: "center",
        width: Device.isTablet ? 400 : 200,
        fontWeight: 600,
        marginBottom: 10,
    },
    heroBtntext: {
        color: "#fff",
        textAlign: "center"
    },
    inputs: {
        width: Device.isTablet ? 500 : 290,
        backgroundColor: '#fff',
        paddingHorizontal: 15,
        paddingVertical: Device.isTablet ? 9 : 6,
        borderRadius: 14,
    },
    btn: {
        width: Device.isTablet ? 120 : 90,
        paddingHorizontal: 15,
        paddingVertical: 8,
        textAlign: "center",
        marginTop: 20,
        backgroundColor: "#3A6070",
        borderRadius: 30
    },
    bookserviceImages: {
        height: Device.isTablet ? 180 : "100%",
        width: Device.isTablet ? 180 : "100%",
    },
    bgContents: {
        top: 60,
        left: 15
    },
    bgText: {
        fontSize: Device.isTablet ? 19 : 14,
    },
    bgText2: {
        fontSize: 30,
        marginTop: 20,
        marginLeft: 5,
        width: 300,
    },
    bgText3: {
        fontSize: Device.isTablet ? 35 : 20,
        marginTop: 30,

    },
    bgsubtext2: {
        fontSize: 14,
        marginTop: 10,
        marginLeft: 5,
    },
    btn2: {
        width: 120,
        paddingHorizontal: 15,
        paddingVertical: 10,
        textAlign: "center",
        marginLeft: 0,
        marginTop: 20,
        backgroundColor: "#000",
        borderRadius: 14,
    },
    serviceHead: {
        marginLeft: 20,
        marginVertical: 30,
        fontSize: Device.isTablet ? 28 : 20,
        fontWeight: 500,
    },
    serviceLayout: {
        flex: 1,
        flexDirection: "row",
        flexWrap: "wrap",
        alignItems: "center",
        justifyContent: "center",
        marginBottom: 20,
    },

    flatlistHead: {
        marginLeft: 20,
        marginVertical: 20,
        fontSize: Device.isTablet ? 28 : 20,
        fontWeight: 500,
        marginTop: 30,
    },
    imageStyle:{
        height:70,
        width:70
    }

})

export default styles