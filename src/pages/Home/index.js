import { View, Text, TextInput, TouchableOpacity, ScrollView, FlatList } from 'react-native'
import React from 'react'
import Background from '../../containers/Background'
import styles from "./styles"
import BG from "../../../assets/bg.png"
import BG2 from "../../../assets/bg2.png"
import experttech from "../../../assets/experttech.png"
import { Banners, services, mostbooked, cleancontrol, Quickrepair } from '../../Data/data'
import Banner from '../../containers/Banners'
import Scrollcard from '../../containers/Cards/Scrollcard'
import Footer from '../../containers/Footer'
import Card from '../../containers/Cards/card'

const Home = ({ navigation }) => {

    return (
        <> 
            <ScrollView>
                <Background imgSource={BG} >
                    <View style={styles.herobgWrapper}>
                        <View style={styles.heroBg}>
                            <Text style={styles.heroTexts}>Help when you need it, at your fingertips</Text>
                        </View>
                        <View style={styles.inputarea}>
                            <TextInput
                                style={styles.inputs}
                                placeholder='What service do you need?'
                            ></TextInput>
                        </View>
                        <TouchableOpacity style={styles.btn}>
                            <Text style={styles.heroBtntext}>Continue</Text>
                        </TouchableOpacity>
                    </View>
                </Background>
                < View>
                    <Text style={styles.serviceHead}> Book Services</Text>
                    <View style={styles.serviceLayout}>
                        {services.map((items) => {
                            return (
                                <Card key={items.id} imageStyle={styles.imageStyle} serviceImage={items.photo}  serviceText={items.title} />
                            )
                        })}
                    </View>
                </View>
                <Banner source={Banners} />
                <View>
                    <Text style={styles.flatlistHead}>Most Booked Services</Text>
                    <View style={styles.serviceLayout}>
                        <FlatList
                            horizontal
                            data={mostbooked}
                            renderItem={(elem) => {
                                return <Scrollcard key={elem.item.id}  ImageSource={elem.item.photo}  title={elem.item.title} ratings={elem.item.rate} Price={elem.item.price} />
                            }}
                        />
                    </View>
                </View>
                <View>
                    <Text style={styles.serviceHead}> Cleaning And Pest Control</Text>
                    <View style={styles.serviceLayout}>
                        {cleancontrol.map((items) => {
                            return (
                                <Card key={items.id} serviceImage={items.photo} serviceText={items.title} />
                            )
                        })}
                    </View>
                </View>
                <Background imgSource={BG2}>
                    <View style={styles.bgContents}>
                        <Text style={styles.bgText2}>Single Wall To Full House</Text>
                        <Text style={styles.bgsubtext2}>We got your back</Text>
                        <TouchableOpacity style={styles.btn2}>
                            <Text style={{ color: "#fff" }}>Upto 15% Off</Text>
                        </TouchableOpacity>
                    </View>
                </Background>
                <Text style={styles.flatlistHead}>Quick Home Repair</Text>
                <View style={styles.serviceLayout}>
                    <FlatList
                        horizontal
                        data={Quickrepair}
                        renderItem={(elem) => {
                            return <Scrollcard key={elem.item.id} ImageSource={elem.item.photo}  title={elem.item.title} ratings={elem.item.rate} Price={elem.item.price} />
                        }}
                    />
                </View>
                <Background imgSource={experttech}>
                    <View style={styles.bgContents}>
                        <Text style={styles.bgText3}>Expert Technician </Text>
                        <Text style={styles.bgText}>At doorstep in 2 Hours </Text>
                    </View>
                </Background>
            </ScrollView>
            <View>
                <Footer
                    homepress={() => navigation.navigate("Home")}
                    categorypress={() => navigation.navigate("Category")}
                    accountpress={() => navigation.navigate("Account")}
                    rewardpress={() => navigation.navigate("Fashion")}
                />
            </View>
        </>
    )
}

export default Home