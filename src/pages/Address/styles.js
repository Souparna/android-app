
import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'

const styles = StyleSheet.create({
    mapStyling: {
        marginTop: 30,
        height: 500,
        backgroundColor: "gray",
    },
    addaddressBtn: {
        width: "90%",
        backgroundColor: "#3C50E0",
        paddingVertical: 12,
        textAlign: "center",
        borderRadius: 12,
        marginBottom: 20,
    },
    addaddressBtnText: {
        color: "#fff",
        textAlign: "center",
    },
    
    addressInputs: {
        marginLeft: 10,
        marginBottom: 30,
    },
    addressInput: {
        borderWidth: 0.4,
        width: "95%",
        borderRadius: 6,
        marginBottom: 8,
        paddingVertical: 8,
        paddingHorizontal: 10,
    },
    addressSaveas: {
        marginLeft: 10,
    },
    addressSaveastitle: {
        fontSize: 18,
        fontWeight: 500,
    },
    addressSaveasbuttons: {
        display: "flex",
        flexDirection: "row",
        marginVertical: 12,
    },
    addaddress: {
        width: "100%",
        display: "flex",
        alignItems: "center",

    },
    accountButton:{
        paddingHorizontal:4,
        paddingVertical:3,
        borderRadius:3,
        borderWidth:0.2,
        marginHorizontal:5,
    }
})

export default styles

