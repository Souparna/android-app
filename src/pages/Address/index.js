import { View, Text, ScrollView, TouchableOpacity, TextInput } from 'react-native'
import React from 'react'
import styles from './styles'
import MapView from 'react-native-maps';
import UserAddress from '../../containers/useraddress';

const Address = ({navigation}) => {
    return (
        <>
            <ScrollView style={styles.container}>
                <View style={styles.mapStyling}>
                    {/* <MapView
                    style={{width:"100%", height:"100%"}}
                        initialRegion={{
                            latitude: 37.78825,
                            longitude: -122.4324,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}
                    /> */}
                  
                </View>
                <UserAddress userName="Asha Nagar" userDescription="  Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione deserunt quidem totam nostrum repudiandae "/>
                <View style={styles.addressInputs}>
                    <TextInput style={styles.addressInput} placeholder='House/ Flat Number' />
                    <TextInput style={styles.addressInput} placeholder='Landmark (Optional)' />
                    <TextInput style={styles.addressInput} placeholder='Name' />
                </View>
                <View style={styles.addressSaveas}>
                    <Text style={styles.addressSaveastitle}>Save as</Text>
                    <View style={styles.addressSaveasbuttons}>
                        <TouchableOpacity style={styles.accountButton}>
                            <Text>Home</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.accountButton}>
                            <Text>Other</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.addaddress}>
                    <TouchableOpacity onPress={() => navigation.navigate("Date")} style={styles.addaddressBtn}>
                        <Text style={styles.addaddressBtnText}>Save and Proceed to slots</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>

        </>
    )
}

export default Address