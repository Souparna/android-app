import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'

const styles = StyleSheet.create({
    leftArrow: {
        height: 25,
        width: 25,
        marginLeft: 20,
        marginBottom: 5,
    },
    addressHead: {
        marginVertical: 20,
        marginLeft: 10,
        width: "90%"
    },
    addressHeadtitleandbutton: {

        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: 10,
    },
    accountTitle: {
        fontSize: 20,
        fontWeight: 500,

    },
    accountButton: {
        borderWidth: 0.4,
        borderRadius: 8,
        paddingHorizontal: 10,
        paddingVertical: 4,
        marginLeft: 4,
        borderColor: "#ddd"
    },
    addressDetails: {
        fontSize: 13,
        marginBottom: 20,
    },


    leftarrowStyle: {
        marginTop: 40,
    },
    arrivalStyles: {
        marginLeft: 10,
        marginTop: 15,
    },
    arrivalHead: {
        fontSize: 18,
        fontWeight: 500,
    },
    arrivalSubtext: {
        fontSize: 13,
        marginBottom: 15,

    },
    arrivalDates: {
        display: "flex",
        flexDirection: "row",
        alignItems:"center",
        justifyContent:"center",
    },
    arrivalButtons: {
        width: 80,
        height: 100,
        borderWidth: 0.5,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        marginLeft: 8,
        borderRadius: 12,
    },
    arrivalDay: {
        fontWeight: 400,
        fontSize: 16,
    },
    arrivalDate: {
        fontWeight: 600,
        fontSize: 24,
    },
    arrivalNotes: {
        width: "auto",
        // marginLeft:10,
        marginVertical: 25,
        display: "flex",
        flexDirection: "row",
        paddingVertical: 15,
        backgroundColor: "#F8F8F8"
    },
    noteSign: {
        fontWeight: 600,
        marginLeft: 10,
        marginRight: 4,
    },
    arrivalNotetext: {
        marginRight: 30,
        fontSize: 13,
        width: "85%"
    },
    time0fservicehead: {
        fontSize: 18,
        fontWeight: 600,
        marginVertical: 20,
        marginLeft: 10,
    },
    serviceDates: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-around",
        flexWrap: "wrap",
        marginBottom: 30,
    },
    serviceDatebutton: {
        width: Device.isTablet ? 220 : 110,
        borderWidth: 0.5,
        paddingVertical: 12,
        marginBottom: 10,
        borderRadius: 10,

    },
    serviceDatebuttonText: {
        textAlign: "center",
        fontSize:
        Device.isTablet ? 18 : 14,
    }  ,
    addAddress: {
        width: "100%",
        display: "flex",
        alignItems: "center",

    },
      addaddressBtn: {
        width: "90%",
        backgroundColor: "#3C50E0",
        paddingVertical: 12,
        textAlign: "center",
        borderRadius: 12,
        marginBottom: 20,
    },
    addaddressBtnText: {
        color: "#fff",
        textAlign: "center",
    }


})

export default styles