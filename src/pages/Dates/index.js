import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import styles from './styles'
import Footer from '../../containers/Footer'
import leftarrow from '../../../assets/leftarrow.png'
import home from "../../../assets/home.png"
import UserAddress from '../../containers/useraddress'
import { slots } from '../../Data/data'

const Date = () => {
    return (
        <>
            <ScrollView style={styles.container}>
                <View style={styles.leftarrowStyle}><Image style={styles.leftArrow} source={leftarrow} /></View>
                <UserAddress userLogo={home} userName="Boro Bazar" userDescription=" Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt, eveniet asperiores alias iusto reiciendis nostrum corporis " />
                <View style={styles.arrivalStyles}>
                    <Text style={styles.arrivalHead} >When should the professional arrive?</Text>
                    <Text style={styles.arrivalSubtext}>Your service will take approx 50 mins</Text>
                    <View style={styles.arrivalDates}>
                        <TouchableOpacity style={styles.arrivalButtons}>
                            <View style={styles.arrival_button}>
                                <Text style={styles.arrivalDay}>Thu</Text>
                                <Text style={styles.arrivalDate}>09</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.arrivalButtons}>
                            <View style={styles.arrivalButton}>
                                <Text style={styles.arrivalDay}>Fri</Text>
                                <Text style={styles.arrivalDate}>10</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.arrivalButtons}>
                            <View style={styles.arrivalButton}>
                                <Text style={styles.arrivalDay}>Sun</Text>
                                <Text style={styles.arrivalDate}>12</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.arrivalNotes}>
                    <Text style={styles.noteSign}>NOTE:</Text>
                    <Text style={styles.arrivalNotetext}>Free Cancellation till 3hours before the booked slot, post that ₹500 chargeable</Text>
                </View>
                <View >
                    <Text style={styles.time0fservicehead}>Select start time of service</Text>
                    <View style={styles.serviceDates}>
                        {
                            slots.map((items) => {
                                return (
                                    <TouchableOpacity style={styles.serviceDatebutton}>
                                        <Text style={styles.serviceDatebuttonText}>{items.title}</Text>
                                    </TouchableOpacity>

                                )
                            })
                        }
                    </View>
                    <View style={styles.addAddress}>
                        <TouchableOpacity style={styles.addaddressBtn}>
                            <Text style={styles.addaddressBtnText}>Proceed to checkout</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </>
    )
}

export default Date