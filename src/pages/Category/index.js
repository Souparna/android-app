import { View, Text, TextInput, ScrollView, Image, FlatList } from 'react-native'
import React from 'react'
import categories from '../../../assets/categories.png'
import { Quickrepair, categorysections, mostbooked, Banners } from '../../Data/data'
import styles from "./styles"
import Banner from "../../containers/Banners"
import Scrollcard from '../../containers/Cards/Scrollcard'
import Footer from '../../containers/Footer'
import Card from '../../containers/Cards/card'


const Category = ({ navigation }) => {
    return (
        <>
            <ScrollView>
                <View style={styles.categoryHead}>
                    <Image style={styles.categoryImg} source={categories}></Image>
                    <Text style={styles.categoryHeadtext}>Catagory</Text>
                </View>
                <View style={styles.categoryAddress}>
                    <Text style={styles.addressHead}>Your Address</Text>
                    <Text style={styles.addressDetails}>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quos, necessitatibus</Text>
                </View>
                <View style={styles.categoryInputarea}>
                    <TextInput
                        style={styles.categoryInput}
                        placeholder='Username'
                    ></TextInput>
                </View>
                < View>
                    <View style={styles.serviceLayout}>
                        {categorysections.map((items) => {
                            return (
                                <Card serviceImage={items.photo} serviceText={items.title} />
                            )
                        })}
                    </View>
                </View>
                <Banner source={Banners} />
                <View>
                    <Text style={styles.flatlistHead}>Most Booked Services</Text>
                    <View style={styles.serviceLayout}>
                        <FlatList
                            horizontal
                            data={mostbooked}
                            renderItem={(elem) => {
                                return <Scrollcard Cardstyle={styles.serviceitems2} ImageSource={elem.item.photo} textstyle={styles.cardtextstyles} title={elem.item.title} ratingstyle={styles.cardratingstyles} ratings={elem.item.rate} Price={elem.item.price} />
                            }}
                        />
                    </View>
                </View>
                <View>
                    <Text style={styles.flatlistHead}>Quick Home Repair</Text>
                    <View style={styles.serviceLayout}>
                        <FlatList
                            horizontal
                            data={Quickrepair}
                            renderItem={(elem) => {
                                return <Scrollcard ImageSource={elem.item.photo} title={elem.item.title} ratings={elem.item.rate} Price={elem.item.price} />
                            }}
                        />
                    </View>
                </View>
            </ScrollView>
            <View>
                <Footer
                    homepress={() => navigation.navigate("Home")}
                    categorypress={() => navigation.navigate("Category")}
                    accountpress={() => navigation.navigate("Account")}
                    rewardpress={() => navigation.navigate("Fashion")}

                />
            </View>
        </>
    )
}

export default Category