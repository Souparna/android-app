import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'

const styles = StyleSheet.create({
    categoryHead: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        marginTop: 80,
        marginLeft: 20,

    },
    categoryImg: {
        marginRight: 10,
        height: 30,
        width: 30,


    },
    categoryHeadtext: {
        fontSize: 20,
        fontWeight: 500,
    },
    categoryproductimg: {
        height: Device.isTablet ? 120 : 70,
        width: Device.isTablet ? 200 : 110,
    },
    categoryAddress: {
        marginLeft: 20,
        marginTop: 20,
    },
    addressHead: {
        fontSize: Device.isTablet ? 28 : 16,
        marginBottom: 8,
    },
    addressDetails: {
        width: Device.isTablet ? 450 : 320,
        fontSize: Device.isTablet ? 16 : 12,
    },
    categoryInputarea: {
        marginHorizontal: 15,
        marginTop: 20,

    },
    categoryInput: {
        borderColor: "#ddd",
        borderWidth: 1,
        paddingHorizontal: 10,
        paddingVertical: 7,
        borderRadius: 10,
    },

    serviceLayout: {
        flex: 1,
        flexDirection: "row",
        flexWrap: "wrap",
        alignItems: "center",
        justifyContent: "center",
        marginBottom: 20,
    },
    flatlistHead: {
        marginLeft: 20,
        marginVertical: 20,
        fontSize: Device.isTablet ? 28 : 20,
        fontWeight: 500,
        marginTop: 30,
    },

})

export default styles
