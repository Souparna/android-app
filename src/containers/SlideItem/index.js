import { View, Text, Image } from 'react-native'
import React from 'react'
import styles from './styles'

const SlideItem = ({ item }) => {
    return (
        <View style={styles.slideitem}>
            <Image style={styles.fashionBackground} source={item.image}></Image>
            <View style={styles.fashionInfo}>
                <Text style={styles.verified}>{item.verify}</Text>
                <View style={styles.fashionHead}>
                    <Text style={styles.fashionheadTitle}>{item.title}</Text>
                    <Image source={item.tag}></Image>
                </View>
                <Text style={styles.festivemode}>{item.mode}</Text>
                <Text style={styles.hasTag}>{item.hastag}</Text>
                <Text style={styles.ProductID}>{item.productid}</Text>
                <Text style={styles.itemCost}>Cost of Item: <Text style={styles.costTotal}> {item.cost}</Text></Text>
            </View>
            <View style={styles.rightDetails}>
                <Image source={item.alert}></Image>
                <View style={styles.views}>
                <Image source={item.view}></Image>
                <Text style={styles.viewsNumber}>8000</Text>
                </View>
                <Image source={item.wp}></Image>
            </View>
        </View>
    )
}

export default SlideItem