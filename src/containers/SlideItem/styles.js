import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'
import { Dimensions } from "react-native";

const { height, width } = Dimensions.get('screen')

const styles = StyleSheet.create({
    fashionBackground: {
        height,
        width,
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },
    fashionInfo: {
        position:"absolute",
        bottom:40,
        left:20,
        
    },
    fashionHead: {
        display: "flex",
        flexDirection: "row",

    },
    verified:{
        color:"#00FF2A"
    },
    fashionheadTitle: {
        fontSize: 28,
        marginRight: 10,
        color:"#fff",
    },
    festivemode: {
        fontSize: 15,
        color:"#fff"
    },
    ProductID: {
        fontWeight: "700",
        marginTop: 5,
        color:"#fff",
    },
    hasTag:{
        color:"#fff"
    },
    itemCost: {
        fontWeight: "700",
        marginTop: 5,
        color:"#fff",
    },
    costTotal:{
        paddingHorizontal: 5,
        paddingVertical:5,
        backgroundColor: "#fff",
        borderRadius:5,
        color:"#000",
        marginLeft:3,
    },
    rightDetails:{
        position:"absolute",
        bottom:30,
        right:20,
        display:"flex",
        alignItems:"center"
    },
    views:{
        display:"flex",
        alignItems:"center",
        marginVertical:13,
    },
    viewsNumber:{
        color:"#fff"
    }


})

export default styles