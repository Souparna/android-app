import { View, Text, TouchableOpacity, KeyboardAvoidingView } from 'react-native'
import React, { useState } from 'react'
import styles from './styles'
import SearchBar from '../../category-contents/searchBar'
import { data, jewelry, others, tailor } from '../../category-contents/SearchData/data'
import Events from '../../category-contents/events'


const Categories = () => {
  const [hideComp, setHideComp] = useState(false)
  const [jewelrydata, setjewlryData] = useState(false)
  const [eventsdata, setEventsData] = useState(false)
  const [tailordata, setTailorData] = useState(false)
  const [other, setOthers] = useState(false)
  const [events, setEvents] = useState(false)
  const visibleComp = (name) => {
    if (name === "clothing") {
      setHideComp(!hideComp)

    } if (name === "jewelry") {
      setjewlryData(!jewelrydata)
    } if (name === 'events') {
      setEventsData(!eventsdata)
    } if (name === "tailor"){
      setTailorData(!tailordata)
    } if (name === "others"){
      setOthers(!other)
    } if (name === "events"){
      setEvents(!events)
    }


  }
  return (
    <>
      <KeyboardAvoidingView style={styles.categories}>
        <TouchableOpacity>
          <Text style={styles.categoryItems} onPress={() => visibleComp("clothing")}>Clothing</Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={styles.categoryItems} onPress={() => visibleComp("jewelry")}>Jewelry</Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={styles.categoryItems} onPress={() => visibleComp("events")}>Events</Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={styles.categoryItems} onPress={() => visibleComp("tailor")}>Tailoring</Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={styles.categoryItems} onPress={() => visibleComp("others")}>Others</Text>
        </TouchableOpacity>

      </KeyboardAvoidingView>
      <View style={styles.searchbar}>
        {

          hideComp ? (
            <SearchBar data={data} />
          ) : null


        }
        {
          jewelrydata ? (
            <SearchBar data={jewelry} />
          ) : null
        }
        {
          tailordata ? (
            <SearchBar data={tailor} />
          ) : null
        }
        {
          other ? (
            <SearchBar data={others} />
          ) : null
        }


      </View>
      <View style={styles.searchbar}>
      {
          events ? (
            <Events />
          ) : null
        }
      </View>

    </>
  )
}

export default Categories 