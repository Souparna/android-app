import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'

const styles = StyleSheet.create({
    categories:{
        position:"absolute",
        top: 110,
        zIndex:999,
        display:"flex",
        width:"100%",
        alignItems:"center",
        flexDirection:"row",
        justifyContent:"space-around"
    },
    categoryItems:{
        color:"#fff",
        fontWeight:"700",
    },
    searchbar:{
        zIndex:999
    }

})

export default styles