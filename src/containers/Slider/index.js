import { View, Text, FlatList, Animated, Dimensions } from 'react-native'
import React, { useRef } from 'react'
import styles from './styles'
import { fashion } from '../../Data/data'
import SlideItem from '../SlideItem'
import Pagination from '../Pagination'
import Header from '../Header'
import Categories from '../categories'
import SearchBar from '../../category-contents/searchBar'

const { width } = Dimensions.get("screen")

const Slider = () => {
    const scrollX = useRef(new Animated.Value(0)).current;
    const handleScroll = event => {
        Animated.event(
            [
                {
                    nativeEvent: {
                        contentOffset: {
                            x: scrollX,
                        },
                    },
                },
            ],
            {
                useNativeDriver: false,
            },
        )(event);
    }
    return ( 
        <View>
            <Header />
            <Categories />
            <FlatList
                data={fashion}
                renderItem={({ item }) => <SlideItem item={item} />}
                horizontal
                pagingEnabled
                showsHorizentalScrollIndicator={false}
                onScroll={handleScroll}
            />
            <View style={styles.pagination}>
                {
                    fashion.map((_, id) => {
                        const inputRange = [(id - 1) * width, id * width, (id + 1) * width];
                        const dotWidth = scrollX.interpolate({
                            inputRange,
                            outputRange: [6, 10, 6],
                            extrapolate: "clamp",
                        })
                        return <Animated.View key={id.toString()} style={[styles.dots, { width: dotWidth }]} />

                    })
                }
            </View>
        </View>
    )
}

export default Slider