import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'


const styles = StyleSheet.create({
    pagination: {
        width: "100%",
        position: "absolute",
        bottom: 20,
        display: "flex",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center"

    },
    dots: {

        height: 6,
        width: 6,
        borderRadius: 3,
        backgroundColor: "#fff",
        marginRight: 4,
    }


})

export default styles