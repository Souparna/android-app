import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'


const styles = StyleSheet.create({
    headerView:{
        width:"100%",
        position:"absolute",
        top:40,
        zIndex:999,
        
    },

    header:{
        width:"100%",
        display:"flex",
        alignItems:"center",
        justifyContent:"space-around",
        flexDirection:"row",
    },
    headerItems:{
        display:"flex",
        alignItems:"center",
        paddingBottom:10
    },
    headerText:{
        fontSize:15,
        fontWeight:600,
        color:"#fff"
    }


})

export default styles