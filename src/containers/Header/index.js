import { View, Text , Image} from 'react-native'
import React from 'react'
import styles from "./styles"
import HOME from "../../../assets/homeHeader.png"
import CUSTOMER from "../../../assets/customerhead.png"
import BOOKMARK from "../../../assets/bookmarkicon.png"

const Header = () => {
  return (
    <View style={styles.headerView}>
      <View style={styles.header}>
        <View style={styles.headerItems}>
            <Image source={HOME}></Image>
            <Text style={styles.headerText}>Home</Text>
        </View>
        <View style={styles.headerItems}>
            <Image  source={BOOKMARK}></Image>
            <Text style={styles.headerText}>Bookmarks</Text>
        </View>
        <View style={styles.headerItems}>
            <Image source={CUSTOMER}></Image>
            <Text style={styles.headerText}>Customer Care</Text>
        </View>
      </View>
    </View>
  )
}

export default Header