import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'

const styles = StyleSheet.create({
    addressHead: {
        marginVertical: 20,
        marginLeft: 10,
        width: "90%"
    },
    addressHeadtitleandbutton: {

        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: 10,
    },
    accountTitle: {
        fontSize: 20,
        fontWeight: 500,

    },
    accountButton: {
        borderWidth: 0.4,
        borderRadius: 8,
        paddingHorizontal: 10,
        paddingVertical: 4,
        marginLeft: 4,
        borderColor: "#ddd"
    },
    addressHeaddetails: {
        fontSize: 13,
        marginBottom: 20,
    },
    accountLogo: {
        margin: 7,

    },
    accountHead: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
    },


})

export default styles