import { View, Text, TouchableOpacity , Image} from 'react-native'
import React from 'react'
import styles from './styles'

const UserAddress = ({userName, userDescription, userLogo}) => {
    return (
        <View style={styles.addressHead}>
            <View style={styles.addressHeadtitleandbutton}>
            <View style={styles.accountHead}>
                            <Image style={styles.accountLogo} source={userLogo} />
                            <Text style={styles.accountTitle}>{userName}</Text>
                        </View>
                <TouchableOpacity style={styles.accountButton}>
                    <Text>Change</Text>
                </TouchableOpacity>
            </View>
            <Text style={styles.addressHeaddetails}>
                {userDescription}
            </Text>
        </View>
    )
}

export default UserAddress