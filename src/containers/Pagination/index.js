import { View, Text, Animated, Dimensions } from 'react-native'
import React from 'react'
import styles from './styles'

const {width} = Dimensions.get("screen")

const Pagination = ({data, scrollX}) => {
  return (
    <View style={styles.pagination}>
      {
        data.map((_, id)=>{
            const inputRange= [(id-1) *width, id *width, (id+1)*width];
            const dotWidth = scrollX.interpolate({
                inputRange,
                outputRange:[6,10,6],
                extrapolate:"clamp",
            })
            return <Animated.View key={id.toString()} style={[styles.dots, {width:dotWidth}]}/>

        })
      }
    </View>
  )
}

export default Pagination