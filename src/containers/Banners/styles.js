import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'

const styles = StyleSheet.create({
    pricings: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        marginLeft: 10,
        marginVertical: 20,
    },
    pricingsBackground: {
        height: Device.isTablet ? 200 : 105,
        width: Device.isTablet ? 350 : 200,
        marginHorizontal: Device.isTablet ? 20: 10,

    },
})

export default styles