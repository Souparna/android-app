import { View, Text, FlatList , Image} from 'react-native'
import React from 'react'
import styles from './styles'

const Banner = ({source}) => {
    return (
        <View>
            <View style={styles.pricings}>
                <FlatList
                    horizontal
                    data={source}
                    renderItem={(elem) => {
                        return <Image style={styles.pricingsBackground} source={elem.item.photo} />
                    }}
                />
            </View>
        </View>
    )
}

export default Banner