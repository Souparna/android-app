import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'


const styles = StyleSheet.create({

    cardButtonposition: {
        display: "flex",
        alignItems: "center"
    },
    bookServiceimages: {
        height: Device.isTablet ? 180 : 98,
        width: Device.isTablet ? 180 : 100,
    },
    serviceItems2: {
        marginVertical: 10,
        marginLeft: Device.isTablet ? 15 : 15,
        display: "flex",
        height: Device.isTablet ? 300 : 180,
        marginBottom: 0,

    },
    cardtextStyles: {
        width: 90,
        marginLeft:3,
        fontSize: Device.isTablet ? 16 : 12,
    },
    cardratingStyles: {
        width: 90,
    },
    cardPriceStyles:{
        marginLeft:3,
    }


})

export default styles