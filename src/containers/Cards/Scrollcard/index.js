import { View, Text, FlatList, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import styles from './styles'

const Scrollcard = ({ 
    ImageSource, 
    title, 
    ratings, 
    Price, 
    Cardstyle, 
    btn, 
    cardbutton, 
    textstyle, 
    ratingstyle, 
    cardWrapper }) => {
    return (
        <View >
            <View style={styles.serviceItems2}>
                <Image source={ImageSource} style={styles.bookServiceimages} />
                <View style={cardWrapper}>
                    <Text style={styles.cardtextStyles}>{title}</Text>
                    <Text style={styles.cardratingStyles}>{ratings}</Text>
                    <Text style={styles.cardPriceStyles}>{'\u20B9'}{Price}</Text>
                    <View style={styles.cardButtonposition}>
                        <TouchableOpacity style={cardbutton}><Text>{btn}</Text></TouchableOpacity>
                    </View>
                </View>
            </View>

        </View>
    )
}

export default Scrollcard