import { View, Text ,Image} from 'react-native'
import React from 'react'
import styles from './styles'


const Card = ({serviceImage,serviceText, imageStyles}) => {
    return (
        <View>
            <View style={styles.serviceItems}>
                <Image source={serviceImage} style={imageStyles}></Image>
                <Text style={styles.itemsStyles}>{serviceText}</Text>
            </View>
        </View>
    )
}

export default Card