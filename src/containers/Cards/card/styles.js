import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'


const styles = StyleSheet.create({
    serviceItems: {
        marginVertical: 26,
        marginHorizontal: Device.isTablet ? 15 : 10,
        display: "flex",
        height: Device.isTablet ? 220 : 100,
        marginBottom: 24,

    },
    itemsStyles: {
        width: Device.isTablet ? 190 : 110,
        fontSize: Device.isTablet ? 16 : 12,
        textAlign: "center",
        marginTop: 12,
        marginBottom: 6,
    },

})

export default styles