import { StyleSheet } from "react-native";
import  Device  from 'react-native-device-detection'


const styles = StyleSheet.create({
    Background: {
        height: Device.isTablet ? 400 : 270,
        width: "100%",
        marginTop: 15,
    },
    backgroundChildren:{
        position: "absolute",
        width:"100%"
    }
})

export default styles