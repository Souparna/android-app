import { View, Text, ImageBackground } from 'react-native'
import React from 'react'
import styles from  "./styles"


const Background = ({imgSource, children}) => {
  return (
    <View>
      <ImageBackground source={imgSource} style={styles.Background}/>
      <View style={styles.backgroundChildren}>
        {children}
      </View>
    </View>
  )
}

export default Background