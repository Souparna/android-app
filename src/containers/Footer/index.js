import { View, Text, Image } from 'react-native'
import React from 'react'
import styles from './styles'
import homeico from '../../../assets/homeico.png'
import categories from '../../../assets/categories.png'
import wallet from '../../../assets/empty-wallet.png'
import profile from '../../../assets/profile.png'

const Footer = ({ homepress, rewardpress, categorypress, accountpress}) => {
  return (
    <View style={styles.footer}>
      <View style={styles.footerContent} >
        <Image source={homeico}></Image>
        <Text onPress={homepress}>Home</Text>
      </View>
      <View style={styles.footerContent}>
        <Image source={categories}></Image>
        <Text onPress={categorypress}>Category</Text>
      </View>
      <View style={styles.footerContent}>
        <Image source={wallet}></Image>
        <Text  onPress={rewardpress}  >Rewards</Text>
      </View>
      <View style={styles.footerContent}>
        <Image source={profile}></Image>
        <Text onPress={accountpress}>Account</Text>
      </View>
    </View>
  )
}

export default Footer