


import { StyleSheet } from "react-native";
import Device from 'react-native-device-detection'

const styles = StyleSheet.create({
    footer: {
        width: "100%",
        height: 50,
        backgroundColor: "#fff",
        display: "flex",
        alignItems: "center",
        justifyContent: "space-around",
        flexDirection: "row",
    
    },
    footerContent: {
        display: "flex",
        alignItems: "center",
    },

})

export default styles