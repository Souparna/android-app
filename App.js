
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from './src/pages/Home';
import Category from './src/pages/Category';
import Account from './src/pages/Account';
import Address from './src/pages/Address';
import Date from './src/pages/Dates';
import Fashion from './src/pages/Fashion';


const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Category" component={Category} />
        <Stack.Screen name="Account" component={Account} />
        <Stack.Screen name="Address" component={Address} />
        <Stack.Screen name="Date" component={Date} />
        <Stack.Screen name="Fashion" component={Fashion} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}


